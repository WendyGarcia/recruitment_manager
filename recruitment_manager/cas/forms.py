from django import forms
from django.contrib.auth.models import User as UserModel


class User(forms.Form):

    username = forms.CharField(label = 'Nombre de usuario', widget = forms.TextInput())
    password = forms.CharField(label = 'Contraseña', widget = forms.TextInput(attrs = {'type': 'password'}))

    def clean(self, *args, **kwargs):
        cleaned_data = super(User, self).clean()
        messages_validation = {}

        try:
            UserModel.objects.get(username = cleaned_data['username'])
        except UserModel.DoesNotExist:
            messages_validation['username'] = ['El usuario no se encuentra registrado en la aplicación']
            raise forms.ValidationError(messages_validation)

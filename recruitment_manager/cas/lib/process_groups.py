from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

def get_user_cas_groups(groups):
    result = None
    if groups.find(","):
        result = groups.split(",")
    else:
        result = groups.split()
    return result


def send_alert_groups_mail(user, groups):
    
    list_destinations = [ 'sgo-desarrollo@emtelco.com.co']
    

    subject = 'Alerta - Grupos SGO que no existen en el proyecto'
    from_email = 'sgo-desarrollo@emtelco.com.co'
    if settings.PROJECT_NAME:
        contexto = {'user': user, 'groups': groups, 'project_name' : settings.PROJECT_NAME}
    else:
        contexto = {'user': user, 'groups': groups}
    text_content = 'El usuario : ' + user + 'Posee los siguientes grupos en LDAP y no existen en la aplicación: ' + ', '.join(groups)
    html_content = render_to_string('mails/group-alerts.html', contexto)  
    msg = EmailMultiAlternatives(subject, text_content, from_email, list_destinations)
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def update_user_groups(user, groups):
    user.groups.clear()
#    user.groups.all().delete()

    groups_does_not_exist= [] # lista con grupos que entran como SGO pero no existen en la APP.
    if groups:
        for group in groups:
            try:
                group_get = Group.objects.get(name = group)
                user.groups.add(group_get)
            except ObjectDoesNotExist:
                groups_does_not_exist.append(group)
        if groups_does_not_exist and settings.ALERT_MAIL_INEXISTENT_GROUPS:
            send_alert_groups_mail(user.username, groups_does_not_exist) # Correo enviado para notificarlo
        if not user.groups.all():
            return None
        else:
            return True
    else:
        return None
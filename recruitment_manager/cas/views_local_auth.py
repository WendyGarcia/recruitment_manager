from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout as django_logout
from django.contrib.auth.models import User as UserModel
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from .forms import User as UserForm

# Create your views here.
class Login(View):

    '''
    Vista para el inicio de sesión del usuario sin CAS, solo SuperAdmins
    '''

    template_name = 'local_auth/login.html'
    breadcrumb = [{'name': 'Inicio de sesión', 'url': None}]

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('index'))
        form = UserForm()
        messages.info(request, "En esta vista solo podrá autenticar superusuarios creados previamente en el proyecto, si se encuentra en un entorno CAS, habilitelo en las configuraciones.")
        return render(request, self.template_name, {
            'form': form, 'breadcrumb': self.breadcrumb})

    def post(self, request, *args, **kwargs):

        username = request.POST['username']
        password = request.POST['password']
        message_incorrect_key = 'Usuario o clave incorrecto'
        message_user_does_not_exist = 'El usuario no se encuentra registrado en la aplicación'
        form = UserForm(request.POST)

        if request.POST.get('next') != reverse('login'):
            return_url = request.POST.get('next')
        elif request.POST.get('referer') != reverse('login'):
            if request.POST.get('referer'):
                return_url = request.POST.get('referer')
            else:
                return_url = reverse('index')
        else:
            return_url = reverse('index')
        if form.is_valid():
            try:
                UserModel.objects.get(username = username)
                if UserModel.objects.get(username = username).is_superuser:
                    auth = authenticate(username = username, password = password)
                    request.session.set_expiry(1800)
                else:
                    messages.error(request, 'Solo es posible autenticar superusuarios')
                    return HttpResponseRedirect(reverse('login'))
                if auth:
                    if auth.is_active:
                        login(request, auth)
                    else:
                        messages.error(request, 'Usuario inactivo')
                else:
                    messages.error(request, message_incorrect_key)
            except UserModel.DoesNotExist:
                messages.error(request, message_user_does_not_exist)
            return HttpResponseRedirect(reverse('login'))
        else:
            return render(request, self.template_name, {'form': form})

def logout(request):
    django_logout(request)
    return HttpResponseRedirect(reverse('login'))

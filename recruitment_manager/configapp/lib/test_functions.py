from django.contrib.auth.models import User
from django.test import Client
from django.contrib.messages.storage.fallback import FallbackStorage
from django.http import HttpRequest

def set_messages(request):
    client = Client()
    session = client.session
    session['test'] = 'test'
    session['positions'] = []
    session.save()
    request.session = session
    messages = FallbackStorage(request)
    setattr(request, '_messages', messages)

def create_request_object(method, **kwargs):
    request = HttpRequest()
    request.method = method

    if 'user_id' in kwargs:
        request.user = User.objects.get(pk = kwargs['user_id'])
    else:
        request.user = User.objects.get(pk = 1)

    if method == 'POST' and 'data' in kwargs:
        for data in kwargs['data']:
            request.POST[data] = kwargs['data'][data]

    return request

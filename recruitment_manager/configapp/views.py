from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import View

# Create your views here.
class BaseView(LoginRequiredMixin, PermissionRequiredMixin, View):

    permission_required = ()

    def handle_no_permission(self):
        messages.error(self.request,'No tiene permiso para realizar la acción')
        return super(BaseView, self).handle_no_permission()

class IndexView(View):

    def get(self, request):
        if settings.PROJECT_NAME:
            project_name = settings.PROJECT_NAME
        else:
            project_name = None
        return render(request, 'index.html', {'project_name': project_name})


class ExampleView(View):

    def get(self, request):
        if settings.PROJECT_NAME:
            project_name = settings.PROJECT_NAME
        else:
            project_name = None
        return render(request, 'example.html', {'project_name': project_name})

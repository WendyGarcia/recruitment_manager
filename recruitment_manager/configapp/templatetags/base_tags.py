from django import template
from django.core.urlresolvers import reverse

register = template.Library()


@register.simple_tag
def get_attribute(instance, attribute):
    '''Returns verbose_name for a model field.'''
    return instance._meta.get_field(attribute).verbose_name.capitalize()

@register.simple_tag
def get_model(instance):
    '''Returns verbose_name for a model.'''
    return instance._meta.verbose_name

@register.simple_tag
def get_field(form, field):
    '''Returns a list with the fields name of a django form.'''
    # Note: Only work to django model forms and django model forms with exclude fields.
    # Code functionality to other attributes of the django forms
    #return list(set([obj.name for obj in form._meta.model._meta.get_fields()]) - set(form._meta.exclude))
    return form[field]


@register.simple_tag(takes_context=True)
def add_active(context, url_name, *args, **kwargs):
    '''Sidebar menu tag (returns active class)'''
    exact_match = kwargs.pop('exact_match', False)

    path = reverse(url_name, args=args, kwargs=kwargs)
    if not exact_match and context.request.path.startswith(path):
        return ' active '
    elif exact_match and context.request.path == path:
        return ' active '
    else:
        return ''


$(document).ready(function (){
    $('.datatable').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      "language": {
              "url": "../static/datatables/lang/Spanish.json"
          }
    })
    $('.select2').select2();
    $('.datepicker').datepicker({
      autoclose: true,
      language: 'es',
      dateFormat: "mm/dd/yy"
    })

    $(".content-wrapper").css({ overflow: "auto" })
    $(".sidebar li.active").parents('li').addClass("active");

    $('.url-redirect').click(function(){
		var url_redirect = $(this).attr('url');
		window.location.href = url_redirect;
	});
});


function set_to_input_hidden(input_id, id, input_text, text) {
    document.getElementById(input_id).value = id;
    document.getElementById(input_text).value = text;
};

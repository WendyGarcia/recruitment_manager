import json
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from middleware import get_current_request


class CustomManager(models.Manager):

    '''
    **DESCRIPCIÓN**
    Esta clase (interfaz) indica un nuevo comportamiento sobre los Query´s\
    que se ejecuten en las clases que hereden de esta.

    :Métodos:
        - **get_queryset**
    '''

    def get_queryset(self):

        '''
        **DESCRIPCIÓN**
        Define el comportamiento de las clase que usen un objeto\
        Manager de tipo CustomManager, siempre que se ejecute una consulta\
        (Query), a dicha consulta se le agregara un filtro sobre el campo\
        "deleted" donde este sea False; Esto hace posible los borrados lógicos\
        de información en base de datos (esto es posible debido a que la clase\
        es de tipo "Manager").

        :returns:
            - retorna el queryset decorado con el filtro sobre el campo "deleted"\
            donde este sea False.
        '''
        return super().get_queryset().filter(disabled=False)


class ModelBase(models.Model):

    '''
    **DESCRIPCIÓN**
    Esta clase define atributos que permiten auditar la informacón que se almacene\
    en las clases que hereden de esta, permite realizar borrados lógicos de la\
    información en la base de datos de forma que el código permanece limpio y\
    sin redundancia de atributos.

    :Atributos:
        - **created_at:** Permite conocer la fecha en que se creo la información,\
        debido a que en el momento de realizar un registro, automaticamente se\
        asigna a este campo el valor de la fecha actual.
        - **modified_at:** Permite conocer la ultima fecha en que se modificó la\
        información, debido a que en el momento de realizar una modificación,\
        automaticamente se asigna a este campo el valor de la fecha actual.
        - **deleted:** Permite realizar borrados lógicos de información en la\
        base de datos, ya que cuando es "borrada" se cambia el valor de este a\
        True. Solo se consulta la información cuyo valor de este campo sea False.
        - **objects:** Este objeto es de tipo "Manager" y lo contienen todas\
        las clases que hereden de clases pertenecientes al módulo "models"\
        (propia de Django), aquí se cambia ese objeto por nuestro propio manager\
        llamado "CustomManager", mediante el cual se cabiara el comportamiento\
        frente a los Query´s, de todas las clases que hereden de "ModelBase".\
        Se relaciona con'CustomManager <models.html#commonpackagesmodels.modelBase.CustomManager>'_

    :Métodos:
        -**logic_delete**

    :Clases:
        -**Meta**
    '''

    created_at = models.DateTimeField('Fecha creación', auto_now_add=True)
    modified_at = models.DateTimeField('Fecha modificación', auto_now=True)
    disabled = models.BooleanField('Inactivar', default=False)
    user_base = models.ForeignKey(User, verbose_name = 'Usuario', editable = False)

    objects = CustomManager()
    alls = models.Manager()

    class Meta:
        '''
        **DESCRIPCIÓN**
        Define la clase principal como una clase abstracta.
        '''
        abstract = True

    def save(self, *args, **kwargs):
        '''
        Asigna el usuario que se encuentra autenticado, es decir, quien realiza
        la acción sobrel el modelo
        '''

        #request = get_current_request()
        self.user_base_id = 1
        return super(ModelBase, self).save(*args, **kwargs)

    def disable(self):
        '''
        Define el valor del atributo "deleted" como True y actualiza la\
        información en la base de datos.
        '''

        self.disabled = True
        self.save(force_update=True)

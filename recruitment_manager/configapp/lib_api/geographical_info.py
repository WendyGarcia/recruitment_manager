from .base import api_rest_request
from .environment_settings import emidware_config


def get_department(id = None):
    if id:
        response = api_rest_request(
            'GET', '/geographical_info/department/{}'.format(id), auth = True, api_settings = emidware_config)
    else:
        response = api_rest_request(
            'GET', '/geographical_info/department/', auth = True, api_settings = emidware_config)
    return response.json()

def get_municipalities(id_department):
    response = api_rest_request(
        'GET', '/geographical_info/department/municipaly/{}'.format(id_department),
        auth = True, api_settings = emidware_config )
    if response:
        return response.json()
    else:
        return None

def get_municipality_detail(id_municipality):
    response = api_rest_request(
        'GET', '/geographical_info/municipality/{}'.format(id_municipality),
        auth = True, api_settings = emidware_config)
    if response:
        return response.json()
    else:
        return None

from .base import api_rest_request
from .environment_settings import cecos_config

def cecos_clients():
    key = cecos_config['token_name']
    response = api_rest_request(
        'GET', '/clients/?{}{}'.format(key, "="), auth = True, api_settings = cecos_config)
    if response:
        return response.json()
    else:
        return None

def cecos_by_client(nit_client):
    response = api_rest_request(
        'POST', '/nit_client/', auth = True, api_settings = cecos_config, data={"nit_client":nit_client})
    if response:
        return response.json()
    else:
        return None

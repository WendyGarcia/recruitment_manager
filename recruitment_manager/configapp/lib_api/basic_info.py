from .base import api_rest_request
from .environment_settings import emidware_config

def get_payroll_type(id = None):
    if id:
        response = api_rest_request(
            'GET', '/basic_info/payroll_type/{}'.format(id), auth = True, api_settings = emidware_config)
    else:
        response = api_rest_request(
            'GET', '/basic_info/payroll_type/', auth = True, api_settings = emidware_config)
    return response.json()

def get_schedule_type(id = None):
    if id:
        response = api_rest_request(
            'GET', '/basic_info/schedule_type/{}'.format(id), auth = True, api_settings = emidware_config)
    else:
        response = api_rest_request(
            'GET', '/basic_info/schedule_type/', auth = True, api_settings = emidware_config)
    return response.json()

def get_contract_type(id = None):
    if id:
        response = api_rest_request(
            'GET', '/basic_info/contract_type/{}'.format(id), auth = True, api_settings = emidware_config)
    else:
        response = api_rest_request(
            'GET', '/basic_info/contract_type/', auth = True, api_settings = emidware_config)
    return response.json()

def get_convocation_type(id = None):
    if id:
        response = api_rest_request(
            'GET', '/basic_info/convocation_type/{}'.format(id), auth = True, api_settings = emidware_config)
    else:
        response = api_rest_request(
            'GET', '/basic_info/convocation_type/', auth = True, api_settings = emidware_config)
    return response.json()

def get_bonding_type(id = None):
    if id:
        response = api_rest_request(
            'GET', '/basic_info/bonding_type/{}'.format(id), auth = True, api_settings = emidware_config)
    else:
        response = api_rest_request(
            'GET', '/basic_info/bonding_type/', auth = True, api_settings = emidware_config)
    return response.json()

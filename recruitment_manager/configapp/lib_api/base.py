import requests
from .response_manager import response_http_code

requests_timeout = 20

def token_validation(api_settings):
    try:
        response = requests.post(
            api_settings['service_name'] + api_settings['token_auth'],
            data = {'username': api_settings['username'], 'password': api_settings['password']})
        if response:
            token_name = api_settings['token_name']
            try:
                return response.json()[0][token_name]
            except:
                return response.json()[token_name]
        else:
            return None
    except:
        return None

def api_rest_request(method, path, auth, api_settings = {}, headers = {}, data = {}, *args):
    token = token_validation(api_settings)
    header = {}
    if token:
        url = api_settings['service_name'] + path
        if not api_settings['auth_in_url']:
            header = {'Authorization': api_settings['token_key'] + ' ' + token}
        else:
            if not '?{}{}'.format(api_settings['token_key'], '=') in path:
                data.update({'key': token })
            else:
                url = url + token
        try:
            if method == 'POST':
                response = requests.post(url, data = data, headers = header, timeout = requests_timeout)
            elif method == 'GET':
                response = requests.get(url, headers = header, timeout = requests_timeout)
        except:
            return response_http_code(408)
        else:
            if response.status_code == requests.codes.ok:
                if response.json():
                    return response
                else:
                    return response_http_code(response = False)
            else:
                return response_http_code(response.status_code)
    else:
        return None

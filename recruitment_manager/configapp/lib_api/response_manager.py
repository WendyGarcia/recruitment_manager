def response_http_code(response):
    if isinstance(response, int) == True:
        return code_message(response)
    elif response == False:
        return code_message(response)

def code_message(response_code):
    if response_code == 204:
        print('Tarea Completada')
    elif response_code == 401:
        print('Problemas en la autenticación')
    elif response_code == 403:
        #raise PermissionDenied
        print('No tiene los permisos necesarios')
    elif response_code == 404:
        print('Recurso no encontrado')
    elif response_code == 405:
        print('Método no disponible')
    elif response_code == 500:
        print('Error interno del servidor')
    elif response_code == 408:
        print('Servicio no disponible')
    elif response_code == False:
        print('Permitido para hacer consultas pero estas no retornan datos (Falta de permisos o de resultados)')
    else:
        print('Error inesperado')

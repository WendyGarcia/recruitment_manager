'''

The following constants are used to parametrize the configuration of
the Auth method.

If this project runs in a CAS authentication enviroment, set ENABLE_CAS_AUTH to
True, else if runs in a local enviroment without access to CAS, set to False and
create a superuser in shell to authenticate in the application.

See the documentation in the shared folder of the  project SGO - CAS client


ENABLE_CAS_AUTH = True * Enable Cas authentications and auto ADD the necessary
                        configurations like middleware, templates , URLS.py,
                        and backend authentication for correc operation.

CAS_SSL_LOCAL_VALIDATION = True *if CAS server serve a SSL conections.
PROJECT_NAME = 'the name of this project'
EMAIL_HOST = '10.1.1.96' *Email relay host, don't modify.
EMAIL_PORT = 25 * Port of Email relay host.
CAS_LOGOUT_COMPLETELY = True *Request a completly logout to CAS server.
CAS_PROVIDE_URL_TO_LOGOUT = True *CAS server provide the redirect logout.
CAS_AUTO_CREATE_USER = True *create any user authorizated by CAS server.
CAS_AUTO_UPDATE_USER_GROUPS = True *Auto update user groups provided by
                                    the CAS server.
ALERT_MAIL_INEXISTENT_GROUPS = True * Email alert any user who has a SGO group
                                    that does not exist in the project.

IN ALL OF THE CONFIG ENVIRONMENTS:
CAS_SERVER_URL = URL OF SECRETS. * Url of the CAS server.

'''
import os
from .base import MIDDLEWARE

ENABLE_CAS_AUTH = True

if ENABLE_CAS_AUTH:
    CAS_SSL_LOCAL_VALIDATION = True
    EMAIL_HOST = '10.1.1.96'
    EMAIL_PORT = 25
    AUTHENTICATION_BACKENDS = [
        'cas.backends.CASBackend'
    ]
    CAS_LOGOUT_COMPLETELY = True
    CAS_PROVIDE_URL_TO_LOGOUT = True
    CAS_AUTO_CREATE_USER = True
    CAS_AUTO_UPDATE_USER_GROUPS = True
    ALERT_MAIL_INEXISTENT_GROUPS = False
    MIDDLEWARE += ['cas.middleware.CASMiddleware']
else:
    LOGIN_URL = '/accounts/login/'
    LOGIN_REDIRECT_URL = '/'

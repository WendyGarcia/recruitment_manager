from .base import *
from .auth import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["10.1.1.85"]

STATIC_ROOT = "/var/www/static/recruitment/"

MEDIA_ROOT = "var/www/media/recruitment/"

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE':       get_secret("ENGINE_DB"),
        'NAME':         get_secret("NAME_DB_TEST"),
        'USER':         get_secret("USER_DB_TEST"),
        'PASSWORD':     get_secret("PASSWORD_DB_TEST"),
        'HOST':         get_secret("HOST_DB_TEST"),
        'PORT':         get_secret("PORT_DB_TEST"),
    }
}

CAS_SERVER_URL = get_secret("AUTH_SERVICE_PRODUCTION")

from .base import *
from .auth import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

STATIC_ROOT = "/var/www/static/recruitment/"

MEDIA_ROOT = "var/www/media/recruitment/"

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE':       get_secret("ENGINE_DB"),
        'NAME':         get_secret("NAME_DB_PRODUCTION"),
        'USER':         get_secret("USER_DB_PRODUCTION"),
        'PASSWORD':     get_secret("PASSWORD_DB_PRODUCTION"),
        'HOST':         get_secret("HOST_DB_PRODUCTION"),
        'PORT':         get_secret("PORT_DB_PRODUCTION"),
    }
}


CAS_SERVER_URL = get_secret("AUTH_SERVICE_PRODUCTION")

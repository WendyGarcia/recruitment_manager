from .base import *
from .auth import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE':       get_secret("ENGINE_DB"),
        'NAME':         get_secret("NAME_DB_LOCAL"),
        'USER':         get_secret("USER_DB_LOCAL"),
        'PASSWORD':     get_secret("PASSWORD_DB_LOCAL"),
        'HOST':         get_secret("HOST_DB_LOCAL"),
        'PORT':         get_secret("PORT_DB_LOCAL"),
    }
}

CAS_SERVER_URL = get_secret("AUTH_SERVICE_LOCAL")

#!/usr/bin/env bash

if [ $1 ]
then
    coverage run --omit=*/envs*,manage.py manage.py test
    echo "Removing previous reports (htmlcov)..."
    rm -rf htmlcov/*
    rmdir htmlcov
    echo "Removed."
    echo "Building coverage report..."
    coverage html
    echo "Coverage report built."
    if [ "$1" = "html" ]
    then
        (firefox htmlcov/index.html &> /dev/null &)
    elif [ "$1" = "report" ]
    then
        coverage report
    fi
    echo "finished"
else
    echo "Need an argument."
fi

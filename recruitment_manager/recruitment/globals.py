from collections import namedtuple

# Constants level process
'''
Dont modify PsychologistRequestStatus actual status options, because the view
PsychologistCharge and her template use this to get filters to show
information
'''
PsychologistRequestStatus = namedtuple(
    'Status',
    ['tobeassigned','assigned','inprogress','onhold','closed','canceled']
)
psychologist_request_status = PsychologistRequestStatus(1,2,3,4,5,6)

from django.db import models
from configapp.models import ModelBase
from django.contrib.postgres.fields import JSONField
from .globals import psychologist_request_status


class PsychologistRequest(ModelBase):
    """
    **DESCRIPCIÓN**
    Formato Cuadro requerimientos por psicólogo subproceso de reclutamiento

    :Herencias:
        - **ModelBase:** Clase asbtracta para la traza de las fechas de \
        modificación, y atributo para la realización de borrados lógicos en \
        lugar de fisicos.

    :Atributos:
        - **id:(PrimaryKey)** Clave primaria y autoincrementable.
        - **request:(ForeignKey)** requerimiento asociado, se relaciona con: \
        `Request<models.html#person.models.Request>`_
        - **psychologist:(ForeignKey)** Psicologo asociado, se relaciona con: \
        `Person<models.html#person.models.Person>`_
        - **request_date:(DateTimeField)** Fecha del requerimiento.
        - **municipality:(ForeignKey)** Municipio asociado al requerimiento: \
        `Municipality<models.html#person.models.Municipality>`_
        - **strategy:(CharField)** Estrategia.
        - **process:(CharField)** Proceso.
        - **delivery_date:(DateField)** Fecha de entrega del requerimiento.
        - **shipping_date:(DateField)** Fecha de envío de candidatos.
        - **close_process_date:(DateField)** Fecha de cierre del proceso.
        - **replace:(CharField)** Reemplaza a.
        - **delivered:(SmallIntegerField)** Número de personas entregadas.
        - **delivery_opportunity:(SmallIntegerField)** Oportunidad de entrega.
        - **status:(SmallIntegerField)** Estado de la solicitud.
        - **closing_opportunity:(SmallIntegerField)** Oportunidad de cierre.
        - **observation:(TextField)** Observaciones.

    :Métodos:
        - **__str__:** Retorna el atributo name de la clase.

    :Clases:
        - **Meta:** Ordena por name los registros y define el verbose_name para \
        la clase.
    """

    date_error_messages = {
        'invalid': 'Ingrese una fecha válida (dd/mm/aaaa)'
    }

    p_r_status_choice =(
        (psychologist_request_status.tobeassigned, 'Por asignar'),
        (psychologist_request_status.assigned, 'Asignado'),
        (psychologist_request_status.inprogress, 'En progreso'),
        (psychologist_request_status.onhold, 'En espera'),
        (psychologist_request_status.closed, 'Finalizado'),
        (psychologist_request_status.canceled, 'Cancelado'))

    request = models.IntegerField('Requerimiento')
    psychologist = models.CharField('Psicólogo', max_length = 30, null = True, blank = True)
    municipality = models.CharField('Municipio', max_length = 30)
    strategy = models.CharField('Tipo de divulgación (Estrategia)', max_length = 30)
    process = models.CharField('Tipo de postulación (Proceso)', max_length = 30)
    delivery_date = models.DateField('Fecha de entrega del requerimiento', error_messages = date_error_messages)
    shipping_date = models.DateField('Fecha de envío de candidatos', error_messages = date_error_messages)
    close_process_date = models.DateField('Fecha de cierre del proceso', error_messages = date_error_messages)
    replace = models.CharField('Reemplaza a', max_length = 30, null = True, blank = True)
    delivered = models.SmallIntegerField('Número de personas entregadas')
    delivery_opportunity = models.SmallIntegerField('Oportunidad de entrega')
    status = models.SmallIntegerField('Estado del requerimiento', blank = True, choices = p_r_status_choice)
    closing_opportunity = models.SmallIntegerField('Oportunidad de cierre')
    observation = models.TextField('Observaciones', null = True, blank = True)


class PractitionerRequest(ModelBase):

    """
    **DESCRIPCIÓN**
    Asignacion de practicante al requerimiento del Psicologo

    """
    date_error_messages = {
        'invalid': 'Ingrese una fecha válida (dd/mm/aaaa)'
    }

    p_r_status_choice =(
        (psychologist_request_status.tobeassigned, 'Por asignar'),
        (psychologist_request_status.assigned, 'Asignado'),
        (psychologist_request_status.inprogress, 'En progreso'),
        (psychologist_request_status.onhold, 'En espera'),
        (psychologist_request_status.closed, 'Finalizado'),
        (psychologist_request_status.canceled, 'Cancelado'))

    practitioner_id = models.CharField('Practicante',max_length = 30, null = True, blank = True )
    psychologist_request_id = models.ForeignKey(PsychologistRequest, verbose_name='Requerimiento')
    status = models.SmallIntegerField('Estado del requerimiento', blank = True, choices = p_r_status_choice)
    observation = models.TextField('Observaciones', null = True, blank = True)


class AspirantRequest(ModelBase):
    """
    **DESCRIPCIÓN**
    Actualizar Checklist del candidato por psicólogo subproceso de reclutamiento

    :Herencias:
        - **ModelBase:** Clase asbtracta para la traza de las fechas de \
        modificación, y atributo para la realización de borrados lógicos en \
        lugar de fisicos.

    :Atributos:
        - **number_identification:(CharField)** Numero Identificacion del candidato.
        - **surname:(CharField)** Primer Apellido del candidato.
        - **second_surname:(CharField)** Segundo Apellido del candidato.
        - **name:(CharField)** Nombres del candidato.
        - **email:(CharField)** Email del candidato.
        - **telephone:(SmallIntegerField)** Número de contacto del candidato.
        - **check_list:(JSONField)** Lista de documentos del candidato.
        - **psychologist_request:(CharField)** Psicologo asociado al requerimiento.

    :Métodos:
        - **__str__:** Retorna el atributo name de la clase.

    :Clases:
        - **Meta:** Ordena por name los registros y define el verbose_name para \
        la clase.
    """
    number_identification = models.CharField('numero Identificación', max_length = 30)
    surname = models.CharField('Primer Apellido', max_length = 30)
    second_surname = models.CharField('Segundo Apellido', max_length = 30)
    name = models.CharField('Nombres', max_length = 30)
    email = models.CharField('Email', max_length = 30)
    telephone = models.SmallIntegerField('Telefono')
    check_list = JSONField()
    psychologist_request = models.ForeignKey(PsychologistRequest, verbose_name = 'psicólogo')
    active_employee = models.BooleanField('Empleado activo', default = False)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "actualizacion checklist"
        verbose_name_plural = "actualizaciones checklist"

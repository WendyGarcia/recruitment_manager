from django.contrib import messages
from django.db.models import Q
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.views.generic import View
from configapp.views import BaseView
from configapp.lib_api.request import get_requests
from ..globals import psychologist_request_status
from ..forms import PsychologistRequest as PsychologistRequestForm
from ..models import PsychologistRequest as PsychologistRequestModel, PractitionerRequest

class PsychologistRequest(BaseView):

    template_name = 'recruitment/psychologist_request_create.html'
    # Note: Define permissions to psychologist assignment
    permission_required = ('recruitment.add_request')

    def get(self, request, *args, **kwargs):
        try:
            requests = get_requests()
        except:
            requests = {}
        form = PsychologistRequestForm(requests = requests)
        return render(request, self.template_name, {'form': form,})

    def post(self, request, *args, **kwargs):
        form = PsychologistRequestForm(request.POST)

        if form.is_valid():
            form.save()
            messages.success(request, 'Se ha creado un sub-requerimiento al requerimiento: ' + 'REQ#%s' % (form.data['request_id'],))
            return redirect(reverse('recruitment:psychologist_requests'))
        else:
            try:
                requests = get_requests()
            except:
                requests = {}
            form = PsychologistRequestForm(request.POST, requests = requests)
            messages.error(request, 'Hubo un problema, revise los campos marcados')
            return render(request, self.template_name, {'form': form})


class PsychologistRequestAssignment(BaseView):

    # Note: Define permissions to psychologist assignment
    permission_required = ('recruitment.add_psychologistrequest')

    def post(self, request, *args, **kwargs):
        obj = get_object_or_404(PsychologistRequestModel, id = kwargs['psychologist_request_id'])
        obj.psychologist = request.POST['psychologist']
        obj.status = psychologist_request_status.assigned
        obj.save(force_update = True)
        messages.success(request, 'Se ha asignado el psicólogo: <strong>%s</strong>, al sub-requerimiento: <strong>#%d</strong>' % (obj.psychologist, obj.id))
        return redirect(reverse('recruitment:psychologist_requests'))


""" Create for: Eliana Marquez 24/08/2017
    clas for list request"""
class ListRequestsPsychologi(View):

    template_name = 'recruitment/list_requests_psyc.html'
    permission_required = ('recruitment.add_psychologistrequest')

    def get(self, request):
        #All request
        requestspsyc = PsychologistRequestModel.objects.filter(request =request.GET['req'])
        for result in requestspsyc:
            result.verbose_status = result.get_status_display()
        return render(request,self.template_name, {'requestspsyc': requestspsyc})


class PsychologistRequestList(View):

    """
        Get list of  all psychologist requests for psychologist
    """

    template_name = 'recruitment/list_psychologist_request.html'

    def get(self, request, **kwargs):

        result_single = None
        results = None
        if kwargs.get('pk'):
            pk = kwargs.get('pk')
            result_single = get_object_or_404(PsychologistRequestModel, pk = pk)
            result_single.verbose_status = result_single.get_status_display()
        else:
            results = PsychologistRequestModel.objects.all()
            for result in results:
                result.verbose_status = result.get_status_display()
        return render(request, self.template_name, {'result': result_single, 'results' : results })


class PsychologistRequestDetail(View):

    template_name = 'recruitment/list_psychologist_detail.html'

    def get(self, request, **kwargs):

        pk = kwargs.get('pk')
        result_single = get_object_or_404(PsychologistRequestModel, pk = pk)
        result_single.verbose_status = result_single.get_status_display()
        return render(request, self.template_name, {'result': result_single})

class PractitionerAssignment(View):

    permission_required = ('recruitment.add_psychologistrequest')

    def post(self, request, *args, **kwargs):
        # obj = get_object_or_404(PractitionerRequest, id = kwargs['psychologist_request_id'])
        # obj.practitioner_id = request.POST['practitioner']
        # obj.status = psychologist_request_status.assigned
        # obj.save(force_update = True)
        messages.success(request, 'Se ha asignado el practicante: <strong>%s</strong>, al sub-requerimiento: <strong>#%d</strong>' % (obj.psychologist, obj.id))
        return redirect(reverse('recruitment:psychologist_requests'))


class PsychologypractitionerList(View):

    template_name = 'requestdesk/list_requests_psyc.html'
    permission_required = ('requestdesk.add_request')

    def GET(self, request, *args, **kwargs):
        pract = ['Practicante 1','Practicante 2','Practicante 3','Practicante 4']
        return render(request, self.template_name, {'pract': pract})

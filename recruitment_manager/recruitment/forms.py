from django import forms
from .models import AspirantRequest
from .globals import psychologist_request_status
from .models import PsychologistRequest as PsychologistRequestModel

class AspirantRequestForm(forms.ModelForm):

    class Meta:
        model = AspirantRequest
        exclude = ['created_at', 'modified_at', 'disabled']


class PsychologistRequest(forms.ModelForm):

    request_text = forms.CharField()
    request_id = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        requests = kwargs.pop('requests', None)
        super(PsychologistRequest, self).__init__(*args, **kwargs)
        if requests:
            requests_choices = [(request['id'], request['code']) for request in requests]  # La lista corresponde a la respuesta de la  API: Listar todos los requerimientos (disponibles para asignación)
        else:
            requests_choices = [(request.id, request) for request in []]

        self.fields['request_text'].required = False
        self.fields['request_id'].required = False
        self.fields['request_text'].label = 'Requerimiento'
        self.fields['request_text'].required = False
        self.fields['request_id'].required = True
        self.fields['request_id'].choices = requests_choices

    def clean(self, *args, **kwargs):
        cleaned_data = super(PsychologistRequest, self).clean()
        messages_validation = {}

        if not 'request_id' in cleaned_data:
            messages_validation['request_text'] = ['Seleccione un requerimiento']
        else:
            del cleaned_data['request_text']
            del self.fields['request_text']

        raise forms.ValidationError(messages_validation)

    def save(self, commit=True, *args, **kwargs):
        if self.instance.psychologist:
            self.instance.status = psychologist_request_status.assigned
        else:
            self.instance.status = psychologist_request_status.tobeassigned

        self.instance.request = int(self.data['request_id'])

        return super(PsychologistRequest, self).save()

    class Meta:
        exclude = ['id', 'user_base', 'created_at', 'modified_at', 'disabled', 'request']

        model = PsychologistRequestModel

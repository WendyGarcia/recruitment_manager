from django.conf.urls import url, include
from .views import view_psychologistapproval
from rest_framework.routers import DefaultRouter
from .views import views_psychologist_request as v_p_r


urlpatterns = [
    url(r'^candidate_search/$', view_psychologistapproval.AspirantRequestCreate.as_view(), name = "candidate_search"),
    url(r'^candidate_search/(?P<cc>\d+)$', view_psychologistapproval.AspirantRequestCreate.as_view(), name = "candidate_search_kwargs"),
    url(r'^candidate_update/$', view_psychologistapproval.AspirantRequestUpdate.as_view(), name = "candidate_update"),
    url(r'^candidate_list/list/$', view_psychologistapproval.AspirantRequestList.as_view(), name = "candidate_list"),
    url(r'^psychologist_request/list/$', v_p_r.PsychologistRequestList.as_view(), name = "psychologist_requests"),
    url(r'^psychologist_request/(?P<pk>\d+)/detail/$', v_p_r.PsychologistRequestDetail.as_view(), name = "psychologist_request_detail"),
    #url(r'^psychologist_request/charge/$', v_p_r.PsychologistCharge.as_view(), name = "psychologist_charges"),
    url(r'^psychologist_request/sub_request/create$', v_p_r.PsychologistRequest.as_view(), name = "psychologist_request_create"),
    url(r'^psychologist_request/sub_request/(?P<psychologist_request_id>\d+)/psychologist_assignment/$', v_p_r.PsychologistRequestAssignment.as_view(), name = "psychologist_request_assignment"),
    url(r'^psychologist_request/assignment/practitioner/$', v_p_r.PractitionerAssignment.as_view(), name ="practitioner_assignment"),
    ]
